module.exports = {
  hex2Ascii(hex) {
    let ascii = "";
    for (let i = 0; i < hex.length && hex.substr(i, 2) !== "00"; i += 2)
      ascii += String.fromCharCode(parseInt(hex.substr(i, 2), 16));

    return ascii;
  },

  ascii2Hex(ascii) {
    let hex = "";
    for (let i = 0; i < ascii.length; i++)
      hex += ascii.charCodeAt(i).toString(16);

    return hex;
  },

  hexToDec(hex) {
    let decimalNo = parseInt(hex.toString(), 16);

    return decimalNo;
  },

  decToHex(dec) {
    let hex = dec.toString(16);

    return hex;
  }
};
