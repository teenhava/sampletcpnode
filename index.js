const net = require('net');
const express = require('express');
const Encoding = require('encoding-japanese');
const { Console } = require('console');
const util = require('./util');
const moment = require('moment');
const port = 3010;
const host = '0.0.0.0'; 

const app = express()
const portExpress = 3000

 

let sockets = [];
let clients = {};
let clientsImei = {};
let socketData = {};
const docmd =  `*SCOS,EV,867584039551853,R0,0,20,123456,1497689816#`;
let onlyOnce = false;
let clientConnection = (sock) => {
    // sock.on('data', function(data1) {
    //     let dataArr = data1.toString().split(',');
    //     let dataHex = data1.toString('hex');
    //     let actualCallback = '';
    //     let command; 
    //     let manufacturer = dataArr[0];
    //     let manufacturerOn2 = dataArr[1];
    //     let operationType = dataArr[1]
    //     let iotType = "";  
    //     let imei=dataArr[2];
    //     let srNo;
    //     let remoteAddressKey = sock.remoteAddress.toString() + sock.remotePort.toString();
    //     console.log('DATA : '+new Date().toString() + sock.remoteAddress + ': ' + data1.toString()); 
 
    //     if( manufacturer === '*SCOS' ){
    //         iotType = 'OM';
    //         command = dataArr[3];
    //         imei = dataArr[2];
    //         actualCallback = data1.toString();

    //     } else if(manufacturer === '*SCOR'   ){
    //         iotType = 'OM';
    //         command = dataArr[3];
    //         imei = dataArr[2];
    //         actualCallback = data1.toString();
    //     }
    //      console.log("OPERATION IS :", command);
    //     if (imei) {
    //         clients[imei] = sock;
    //         clientsImei[remoteAddressKey] = imei;
    //         let cmd = `'*SCOS,EV,867584039551853,D0#`;
    //         sock.write(cmd);
    //     }
          
    //      let data = {
    //          imei: imei,
    //          mode: 'D'
    //      };
    //      data.omniCode = dataArr[1];
        
    //     let iotRequest = {
    //         request: { imei: imei },
    //         data: command,
    //         manufacturer: 'OMNI_TCP_SERVER',
    //         imei: imei,
            
    //     };
    //     console.log('HARSSS server: res'+remoteAddressKey,data); 
    //     let message = `${command} published to Scooter: ${imei}`;
    //     iotRequest.response = { message };
         
    //     console.log(iotRequest.response.message);
         
 
    // });

    // sock.on('close', function(data) {
    //     let index = sockets.findIndex(function(o) {
    //         return o.remoteAddress === sock.remoteAddress && o.remotePort === sock.remotePort;
    //     })
    //     if (index !== -1) sockets.splice(index, 1);
    //     console.log('CLOSED: ' + sock.remoteAddress + ' ' + sock.remotePort);
    // });

    // // Handle Client connection error.
    // sock.on('error', (error) => {
    //     console.error(
    //         `${sock.remoteAddress}:${sock.remotePort} Connection Error ${error}`
    //     );
    // });
}

const tcpServer = net.createServer(clientConnection);
tcpServer.listen(port,host, () => {
    console.log(`TCP Server started on port ${port}`);
});
function DecodeHexStringToByteArray (hexString) {
  let result = [];
 // result.push(0xFFFF);
  while (hexString.length >= 2) { 
      result.push(parseInt(hexString.substring(0, 2), 16));
      hexString = hexString.substring(2, hexString.length);
  }
  return result;
}
function toHexString(byteArray) {
  return Array.from(byteArray, function(byte) {
    return ('0' + (byte & 0xFF).toString(16)).slice(-2);
  }).join('')
}
function apendHex(hexString) {
  //let cmd = `*SCOS,EV,867584039551853,R0,0,20,123456,1497689816#`;
  // let cmdBytes = util.ascii2Hex(hexString);
  // let cmds = util.hex2Ascii(util.ascii2Hex(0xffff) + cmdBytes);
  let cmdBytes = DecodeHexStringToByteArray(hexString);
let cmds = toHexString(cmdBytes);
  return cmds;
}
tcpServer.on('connection',  (socket) => { 
    var clientAddress = `${socket.remoteAddress}:${socket.remotePort}`; 
    console.log(`new client connected: ${clientAddress}`); 
    sockets.push(socket); 
    socket.on("data", (data) => {
        console.log(`Client ${clientAddress}: ${data}`);
        let dataArr = data.toString().split(',');
        let dataHex = data.toString('hex');
        let actualCallback = '';
        let command; 
        let manufacturer = dataArr[0];
        let manufacturerOn2 = dataArr[1];
        let operationType = dataArr[1]
        let iotType = "";  
        let remoteAddressKey = socket.remoteAddress.toString() + socket.remotePort.toString();
        let imei="";
        if(  manufacturer === '*SCOS' ||   manufacturer === '*CMDS' ){
             iotType = 'OM';
             command = dataArr[4];
             imei = dataArr[2];
             actualCallback = data.toString()
         } else if(  manufacturer === '*SCOR' || manufacturer === '*CMDR'  ){
             iotType = 'OM';
             command = dataArr[4];
             imei = dataArr[2];
             actualCallback = data.toString();
         }
          console.log("OPERATION IS :", command);
         if (imei) {
             clients[imei] = socket;
             clientsImei[remoteAddressKey] = imei;
             let timstamp= currTimeInYearForIot();
             let date = new Date();
             let secStamp = Math.floor(date.getTime() / 1000);
             console.log("ACTUAL Sended CMD Timestamp",timstamp);
             let cmd = "";
            //  if(!onlyOnce){
            //   cmd = `*CMDS,EV,863921032828447,${timstamp},L0,0,2345345,${secStamp}#`;
            //   onlyOnce = true;
            //  } else {
            //   cmd = `*CMDS,EV,863921032828447,${timstamp},L1,2345345,${secStamp},3#`;
            //  }
           // cmd = `*SCOS,EV,867584039551853,R0,0,15,1,${secStamp}#`;
            cmd = `*CMDS,EV,863921032828447,${timstamp},D0#`;
           
            //let cmd = `*CMDS,EV,863921032828447,${timstamp},K0,0#`;
             
             let cmd2 = `*CMDS,EV,863921032828447,${timstamp},L0,0,234345,1633640370#`;
             socket.write(cmd);


             //socket.write(cmd2);
            console.log("ACTUAL Sended CMD",cmd);
             //console.log("ACTUAL Sended CMD",cmd2);
         }
        // Write the data back to all the connected, the client will receive it as data from the server
        // sockets.forEach((sock) => {
          
        //   console.log("BEFORE ");
        //   let cmd = `*SCOS,EV,867584039551853,R0,0,20,123456,1497689816#`;
        //   let actualCmd = apendHex(cmd);
        //   sock.write(actualCmd);
        //   console.log("ACTUAL R0 CMD",actualCmd);
        //   cmd = `*SCOS,EV,867584039551853,D1,60#`;
        //   actualCmd = apendHex(cmd);
        //   sock.write(actualCmd);
        //   console.log("ACTUAL D1 CMD",actualCmd);
        //   // console.log("BEFORE "+sock.remoteAddress + ":" + sock.remotePort + " said " + data + "\n");
        //   // sock.write("0xFFFF*SCOS,EV,867584039551853,R0,0,20,123456,1497689816#\r\n");
        //   // sock.write("*SCOS,EV,867584039551853,R0,0,20,123456,1497689816#\r\n");
        //   // sock.write("0xFFFF*SCOS,OM,867584039551853,R0,0,20,123456,1497689816#\r\n");
        //   // sock.write("*SCOS,OM,867584039551853,R0,0,20,123456,1497689816#\r\n"); 

        //   // sock.write("0xFFFF*SCOS,EV,867584039551853,D1,60#\r\n");
        //   // sock.write("*SCOS,EV,867584039551853,D1,60#\r\n");
        //   // sock.write("0xFFFF*SCOS,OM,867584039551853,D1,60#\r\n");
        //   // sock.write("*SCOS,OM,867584039551853,D1,60#\r\n");

        //   // sock.write("0xFFFF*SCOS,EV,867584039551853,D0#\r\n");
        //   // sock.write("*SCOS,EV,867584039551853,D0#\r\n");
        //   // sock.write("0xFFFF*SCOS,OM,867584039551853,D0#\r\n");
        //   // sock.write("*SCOS,OM,867584039551853,D0#\r\n");
        //   //console.log("AFTER "+sock.remoteAddress + ":" + sock.remotePort + " said " + data + "\n");
        //ÿÿ*SCOS,EV,867584039551853,D0#

        // });
     }); 
     socket.on('close', function(data) {
        let index = sockets.findIndex(function(o) {
            return o.remoteAddress === socket.remoteAddress && o.remotePort === socket.remotePort;
        })
        if (index !== -1) sockets.splice(index, 1);
        console.log('CLOSED: ' + socket.remoteAddress + ' ' + socket.remotePort);
    });

    // Handle Client connection error.
    socket.on('error', (error) => {
        console.error(
            `${socket.remoteAddress}:${socket.remotePort} Connection Error ${error}`
        );
    });
    });
    function currTimeInYearForIot(date = null) {
      let time = moment();
      if (date) {
          time = moment(date);
      }
      let currTimeInYear = time.utcOffset(0).format('YYMMDDHHmmss');

      return currTimeInYear;
  }

app.get('/hello', (req, res) => {
    console.log('REQUEST::'+new Date().toString(),req.body);

    const client = new net.Socket();
    const imei= req.query.imei;
    // client.connect(port,function() {
    //   console.log(' Clinet  Connected');
    //   console.log(" Valid SocketIP "+clients[imei].remoteAddress+"  Port: "+clients[imei].remotePort+" Address KEY"+clients[imei].remoteAddressKey);
    //   //client.write("*SCOS,EV,867584039551853,D1,60#\r\n");// *SCOS,OM,867584039551853,D1,60#
    //   client.write("0xFFFF*SCOS,EV,867584039551853,R0,0,20,0,1497689816#\r\n");// *SCOS,OM,867584039551853,D1,60#
    //   clients[imei].write("0xFFFF*SCOS,EV,867584039551853,R0,0,20,0,1497689816#\r\n");
    // });
    
    //clients[imei].write("*SCOS,EV,867584039551853,D1,60#\r\n");// *SCOS,OM,867584039551853,D1,60#

    if (!clients[imei]) {
        res.send( "NO DEVICE IS CONNECTED"); 
    } else {
        console.log(" existing "+clients[imei].remoteAddress+"  Port: "+clients[imei].remotePort+" Address KEY"+clients[imei].remoteAddressKey);
        let cmd = `*SCOS,OM,867584034123245,D0#\r\n`;

        clients[imei].write( cmd, ()=>{
            console.log("Inside after write"+ cmd);
        });// *SCOS,OM,867584039551853,D1,60#
        res.send(cmd);
    } 

  });

//   function converttohex(x) {return x.charCodeAt(0);}
//   function stringFromArray(data)
//   {
//     var count = data.length;
//     var str = "";
    
//     for(var index = 0; index < count; index += 1)
//       str += String.fromCharCode(data[index]);
    
//     return str;
//   }

//   function byteArrayToString(byteArray){

//     // Otherwise, fall back to 7-bit ASCII only
//     var result = "";
//     for (var i=0; i<byteArray.byteLength; i++){
//         result += String.fromCharCode(byteArray[i])
//     }/*from   w  ww . ja v a 2 s .  co  m*/
//     return result;
// }
// function bin2string(array){
// 	var result = "";
// 	for(var i = 0; i < array.length; ++i){
// 		result+= (String.fromCharCode(array[i]));
// 	}
// 	return result;
// }
// function __arrayToString (input) {
//   var i, str = '';

//   for (i = 0; i < input.length; i++) {
//       str += '%' + ('0' + input[i].toString(16)).slice(-2);
//   }
   
//   return str;
// }

// let cmd = `*SCOS,EV,867584039551853,D0#`;
// let cmdBytes = [];
// // cmdBytes.push(-1);
// // cmdBytes.push(-1);
 

// function byteToHex(b) {
//   return hexChar[(b >> 4) & 0x0f] + hexChar[b & 0x0f];
// }
// cmdBytes.push(cmd.split('').map(converttohex)) ;
// const cmds = bin2string(cmdBytes);
// console.log('ASCITOHEX::::    '+cmdBytes);
// console.log('HEXTOASCII::::   '+cmds);


  app.listen(portExpress, () => {
    console.log(`Example app listening at http://localhost:${portExpress}`)
  });